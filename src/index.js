import navigation from './services/navigation';

window.onload = function() {
    var appRoot = document.getElementById('app-root');
    if (appRoot) {
        navigation.setRoutes(appRoot);
    }
};