import m from 'mithril';

function TodosModel() {
    let todos = [];
    let idCounter = 1;
    var TODOS_LOCAL_STORAGE_KEY = 'locally-stored-todos';

    this.loadList = function() {
        // load list from localStorage and setId counter value + 1
        if (!todos || !todos.length) {
            var locallyStoredTodos = localStorage.getItem(TODOS_LOCAL_STORAGE_KEY);
            if (locallyStoredTodos) {
                todos = JSON.parse(locallyStoredTodos);
                idCounter = todos.length + 1;
            }
        }
        return todos;
    };

    this.getList = function(a) {
        return todos;
    };

    this.create = function(data) {
        let todo = {
            title: data.title,
            tasks: data.tasks,
            id: idCounter++,
            timestamp: new Date()
        };

        todos.push(todo);
        localStorage.setItem(TODOS_LOCAL_STORAGE_KEY, JSON.stringify(todos));
        return todo;
    };

    this.deleteTodo = function(id) {
        var index = todos.findIndex(todo => todo.id == id);
        if (index > -1) {
            todos.splice(index, 1);
            localStorage.setItem(TODOS_LOCAL_STORAGE_KEY, JSON.stringify(todos));
        }
    };

    this.updateTodo = function(todoObj) {
        var index = todos.findIndex(todo => todo.id == todoObj.id);
        if (index > -1) {
            todos.splice(index, 1, todoObj);
            localStorage.setItem(TODOS_LOCAL_STORAGE_KEY, JSON.stringify(todos));
        }
    }

    this.todoVm = function() { // todo view model
        return {
            timestamp: null,
            title: '',
            tasks: [this.taskVM()]
        };
    };

    this.taskVM = function() { // task view model
        return {
            label: '',
            isDone: false
        };
    };
}

let todosModel = new TodosModel();

export default todosModel;