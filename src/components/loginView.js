import m from 'mithril';
import auth from '../services/auth';
import navigation from '../services/navigation';

let loginView = () => {
    function login(e) {
        e.preventDefault();
        var username = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        if (username && password) {
            auth.login(username, password)
            .then((res) => {
                navigation.goToHome();
            })
            .catch((err) => {
                alert(err);
            });
        } else {
            alert('Please enter valid credentials.');
        }
    }

    return {
        oninit: vnode => {
            if (auth.isLoggedIn()) {
                navigation.goToHome();
            }
        },
        view: vnode => {
            return [
                m('.main-container flex flex-column justify-center items-center bg-near-white vh-100', [
                    m('h1.main-heading helvetica w-100 tc', 'Login'),
                    m('.login-form flex w5', 
                        m('form.measure.center w-100', [
                            m('div.mv3', [
                                m('label.db fw6 lh-copy f6 helvetica', {for: 'username'}, 'Username'),
                                m('input.pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100', {type: 'text', name:'username', id:'username', placeholder: 'admin'})
                            ]),
                            m('div.mv3', [
                                m('label.db fw6 lh-copy f6 helvetica', {for: 'password'}, 'Password'),
                                m('input.pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100', {type: 'password', name:'password', id:'password', placeholder: '*****'})
                            ]),
                            m('button.b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib', {type: 'submit', onclick: login}, 'Sign in')
                        ])
                    )
                ])
            ];
        }
    }
};

export default loginView;