import m from 'mithril';
import todos from '../models/todos';

let listView = function() {
    var todo = {};

    function updateTaskState(e) {
        e.preventDefault();
        var taskToUpdate = todo.tasks.find(task => task.label == e.target.value);
        if (taskToUpdate) {
            taskToUpdate.isDone = !taskToUpdate.isDone;
            todos.updateTodo(todo);
        }
    }

    function deleteTodo(e) {
        e.preventDefault();
        todos.deleteTodo(todo.id);
    }

    return {
        oninit: vnode => {
            todo = vnode.attrs.todo;
        },
        onupdate: vnode => {
            todo = vnode.attrs.todo;
        },
        view: vnode => {
            return m('.list-container dib w5 mw5 ma3 bg-white br3 pa3 mv3 ba b--black-10 sans-serif', 
                m('.title center fw5', vnode.attrs.todo.title),
                m('.tasks-list mt3', vnode.attrs.todo.tasks.map(task => {
                    return m('.task mt2 flex justify-between', 
                        m('span.label', { class: task.isDone ? 'strike' : '' }, task.label),
                        m('label.relative.flex.items-center',
                            m('input.absolute z-5 w-100 h-100 o-0 input-reset pointer checkbox', {
                                type: 'checkbox',
                                checked: task.isDone,
                                value: task.label,
                                onclick: updateTaskState
                            }),
                            m('span.relative z-4 dib w1 h1 bg-mid-gray overflow-hidden br1 v-mid bg-animate bg-center checkbox-wrapper')
                        )
                    );
                })),
                m('.date-delete-btn-container mt4 flex flex-row justify-between items-center', 
                    m('span.f6.fw3', 'Created on: ' + (new Date(vnode.attrs.todo.timestamp)).toLocaleDateString()),
                    m('button.dib.input-reset.bn.grow bg-transparent pa0 shadow-hover', {type: 'button', onclick: deleteTodo}, m('i.fa.fa-trash-alt'))
                )
            );
        }
    };
};

export default listView;