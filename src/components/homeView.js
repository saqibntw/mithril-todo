import m from 'mithril';
import auth from '../services/auth';
import todos from '../models/todos';

import listView from './listView';
import navigation from '../services/navigation';

function goToCreate() {
    navigation.goToCreateTodo();
}

let homeView = () => {
    return {
        oninit: vnode => {
            if (!auth.isLoggedIn()) {
                navigation.goToLogin();
            }
            todos.loadList();
        },
        view: vnode => {
            return [
                m('.main-container w-100 vh-100', 
                    m('.main-header h3 mw8 mt7 ml7 mr7 flex flex-row justify-between items-center', 
                        m('h1.main-heading dib helvetica', 'Lists'),
                        m('button.b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib', {type: 'button', onclick: goToCreate}, 'Create')
                    ),
                    m('.todos-container ml7', 
                        todos.getList().map(todo => {
                            return m(listView, {todo: todo});
                            
                        })
                    ),
                    !todos.getList().length ? m('h4.mw8 mt4 ml7 fw4 helvetica', 'No lists to show') : ''
                )
            ];
        }
    };
};

export default homeView;