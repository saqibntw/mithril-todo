import m from 'mithril';
import navigation from '../services/navigation';
import todosModel from '../models/todos';

let createView = () => {
    let todoVM = {};
    function updateTitle() {
        todoVM.title = (document.getElementById('Title')).value;
    }

    function updateTask(e) {
        todoVM.tasks[e.target.attributes.index.value].label = e.target.value;
    }

    function addTask() {
        todoVM.tasks.push(todosModel.taskVM());
    }

    function createTodo() {
        todosModel.create(todoVM);
        navigation.goToHome();
    }

    return {
        oninit: vnode => {
            todoVM = todosModel.todoVm();
        },
        view: vnode => {
            return [
                m('.main-container flex flex-column  items-center bg-near-white vh-100', [
                    m('h1.main-heading mt7 helvetica w-100 tc', 'Create Todo'),
                    m('.login-form flex w5', 
                        m('form.measure.center w-100', [
                            m('div.mv3', [
                                m('label.db fw6 lh-copy f6 helvetica', {for: 'Title'}, 'Title'),
                                m('input.pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100', 
                                    {type: 'text', name:'Title', id:'Title', placeholder: 'Groceries...', onchange: updateTitle})
                            ]),

                            m('div.mv3', [
                                m('label.fw6 lh-copy f6 helvetica', {for: 'Tasks'}, 'Tasks'),
                                m('button.dib.ml3 v-btm input-reset.bn.grow pa0 shadow-hover bg-transparent', 
                                    {type: 'button', onclick: addTask}, m('i.fa.fa-plus-square')),
                                m('ul', todoVM.tasks.map((task, index) => {
                                    return m('li.mt2', m('input.pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100', 
                                                {type: 'text', name:'Tasks', id:'Tasks', placeholder: 'Buy a dozen eggs', index: index, onchange: updateTask}));
                                }))
                            ]),
                            m('button.b ph3 pv2 input-reset bg-near-black white ba b--black grow pointer f6 dib', {type: 'button', onclick: createTodo}, 'Create'),
                            m('button.b ph3 pv2 ml2 input-reset ba b--black bg-transparent grow pointer f6 dib', {type: 'button', onclick: navigation.goToHome}, 'Cancel')
                        ])
                    )
                ])
            ];
        }
    }
};

export default createView;