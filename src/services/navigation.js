import m from 'mithril';

import homeView from '../components/homeView';
import loginView from '../components/loginView';
import createView from '../components/createView';

function goToHome() {
    m.route.set('/');
}

function goToLogin() {
    m.route.set('/login');
}

function goToCreateTodo() {
    m.route.set('/create');
}

function setRoutes(root) {
    m.route(root, '/', {
        '/': homeView,
        '/login': loginView,
        '/create': createView
    });
}

export default {
    goToHome,
    goToLogin,
    goToCreateTodo,
    setRoutes
};