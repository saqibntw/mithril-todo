
function Auth() {
    var USER_STATUS_LOCAL_KEY = 'user-logged-in-state';
    this._isLoggedIn = false;

    var storedUserLoggedInState = localStorage.getItem(USER_STATUS_LOCAL_KEY);
    if (storedUserLoggedInState) {
        this._isLoggedIn = Boolean(storedUserLoggedInState);
    }

    this.isLoggedIn = function() {
        return this._isLoggedIn;
    };

    this.login = function(username, password) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (username && username == 'admin' && password && password == '123456') {
                    this._isLoggedIn = true;
                    localStorage.setItem(USER_STATUS_LOCAL_KEY, 'true');
                    resolve('logged in.');
                } else {
                    reject('username or password incorrect.');
                }
            }, 100);
        });
    }
}

let auth = new Auth();

export default auth;